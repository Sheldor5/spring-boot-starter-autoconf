# Spring Boot Starter Autoconfigure

This project brings enhancements to the **[Spring Boot Starter](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using-boot-starter)** dependencies.

## [Logging](logging/)

- Mapped Diagnostic Context (MDC)

## [Tomcat](tomcat/)

- AJP Connector

## [Hazelcast](hazelcast/)

- `application.properties`-driven Hazelcast configuration
- HTTP Session Replication

## [Persistence](persistence/)

- Common Persistence Classes
- Database Schema Versioning with FlyWay

## TODO

- finish Hazelcast module
- finish Persistence module

## Changelog

#### 0.0.1

- first release
