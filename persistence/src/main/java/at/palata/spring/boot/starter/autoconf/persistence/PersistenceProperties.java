package at.palata.spring.boot.starter.autoconf.persistence;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ConfigurationProperties(prefix = Constants.PROPERTY_PREFIX)
public class PersistenceProperties {
}
