package at.palata.spring.boot.starter.autoconf.persistence;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;

import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * @author Michael Palata
 * @date 26.11.2019
 */
@Getter
@Setter
public abstract class AbstractEntity<T> {

	@CreatedDate
	protected LocalDateTime createdAt;

	@LastModifiedDate
	protected LocalDateTime lastModified;

	@Version
	protected Long version;

	abstract T getId();
	abstract void setId(final T id);
}
