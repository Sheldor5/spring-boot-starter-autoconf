package at.palata.spring.boot.starter.autoconf.persistence;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Map;

@Configuration
@ConditionalOnProperty(name = Constants.MODULE_ENABLED_PROPERTY_NAME, havingValue = Constants.MODULE_ENABLED_PROPERTY_VALUE)
@EnableConfigurationProperties(PersistenceProperties.class)
@Slf4j
public class PersistenceConfiguration {

	private final PersistenceProperties properties;

	@Autowired
	public PersistenceConfiguration(final PersistenceProperties properties) {
		this.properties = properties;
	}

	@PostConstruct
	public void initialize() {
		log.debug("Activating {} ...", Constants.MODULE_NAME);
	}
}
