package at.palata.spring.boot.starter.autoconf.persistence;

public class Constants {

	public static final String MODULE_NAME = "Spring Boot Starter Autoconf - Persistence";

	public static final String PROPERTY_PREFIX = "persistence";
	public static final String MODULE_ENABLED_PROPERTY_NAME = PROPERTY_PREFIX + ".enabled";
	public static final String MODULE_ENABLED_PROPERTY_VALUE = "true";
}
