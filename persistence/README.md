# Spring Boot Starter Autoconfigure - Persistence

## Spring Data Configuration

```
spring.datasource.driver-class-name=<jdbc-driver-class-name>
spring.datasource.username=mysqluser
spring.datasource.password=mysqlpass
spring.datasource.url=jdbc:mysql://localhost:3306/myDb?createDatabaseIfNotExist=true
```

## FlyWay Configuration

```
TODO
```
