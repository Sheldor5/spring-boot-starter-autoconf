package at.palata.spring.boot.starter.autoconf.example.listener;

import at.palata.spring.boot.starter.autoconf.example.config.HazelcastConstants;
import com.hazelcast.core.HazelcastInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationReadyEventListener implements ApplicationListener<ApplicationReadyEvent> {

	@Value("${server.port}")
	private int port;
	@Value("${server.servlet.context-path:}")
	private String context;

	private final HazelcastInstance hazelcastInstance;

	@Autowired
	public ApplicationReadyEventListener(final HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		log.info("Running Startup Hook ...");
		hazelcastInstance.getMap(HazelcastConstants.NODES_MAP_NAME)
				.put(HazelcastConstants.NODE_UUID, "http://127.0.0.1:" + this.port + this.context);
	}
}
