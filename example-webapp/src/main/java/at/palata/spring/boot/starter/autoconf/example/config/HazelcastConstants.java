package at.palata.spring.boot.starter.autoconf.example.config;

import java.util.UUID;

public class HazelcastConstants {
	public static final String NODES_MAP_NAME = "nodes";
	public static final String NODE_UUID = UUID.randomUUID().toString();
}
