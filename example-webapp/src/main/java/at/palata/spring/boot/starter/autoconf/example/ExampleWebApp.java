package at.palata.spring.boot.starter.autoconf.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "at.palata")
public class ExampleWebApp {

	public static void main(final String[] args) {
		SpringApplication.run(ExampleWebApp.class, args);
	}
}
