package at.palata.spring.boot.starter.autoconf.example.listener;

import at.palata.spring.boot.starter.autoconf.example.config.HazelcastConstants;
import com.hazelcast.core.HazelcastInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationShutdownEventListener implements DisposableBean {

	private final HazelcastInstance hazelcastInstance;

	@Autowired
	public ApplicationShutdownEventListener(final HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	@Override
	public void destroy() {
		log.info("Running Shutdown Hook ...");
		hazelcastInstance.getMap(HazelcastConstants.NODES_MAP_NAME).remove(HazelcastConstants.NODE_UUID);
	}
}
