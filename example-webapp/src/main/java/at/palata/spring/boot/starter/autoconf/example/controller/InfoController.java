package at.palata.spring.boot.starter.autoconf.example.controller;

import at.palata.spring.boot.starter.autoconf.example.config.HazelcastConstants;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(path = "/info")
public class InfoController {

	private final HazelcastInstance hazelcastInstance;

	@Autowired
	public InfoController(final HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	@GetMapping
	public String info(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		model.addAttribute("request", request);
		model.addAttribute("response", response);

		model.addAttribute("uuid", HazelcastConstants.NODE_UUID);
		model.addAttribute("nodes", hazelcastInstance.getMap("nodes").values());

		return "info";
	}
}
