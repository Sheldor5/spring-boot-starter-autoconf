package at.palata.spring.boot.starter.autoconf.test.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Michael Palata
 * @date 28.11.2019
 */

@Controller
@RequestMapping(TestController.PATH)
@Slf4j
public class TestController {

	public static final String PATH = "/";
	public static final String RESPONSE_VALUE = "Hello World!";

	@GetMapping
	public ResponseEntity<String> get() {
		log.info("Log output from " + this.getClass().getName());
		return ResponseEntity.ok(TestController.RESPONSE_VALUE);
	}
}
