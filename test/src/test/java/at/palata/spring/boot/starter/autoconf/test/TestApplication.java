package at.palata.spring.boot.starter.autoconf.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Michael Palata
 * @date 22.11.2019
 */
@SpringBootApplication(scanBasePackages = "at.palata")
public class TestApplication {
	public static void main(final String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}
}
