package at.palata.spring.boot.starter.autoconf.tomcat;

import at.palata.spring.boot.starter.autoconf.test.TestApplication;
import org.apache.catalina.connector.Connector;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("TOMCAT")
public class AjpTest {

	@Autowired
	private TomcatProperties tomcatProperties;

	@Autowired
	private TomcatConfiguration tomcatConfiguration;

	@Autowired
	private TomcatServletWebServerFactory tomcatServletWebServerFactory;

	@Test
	public void testAjp() {
		Assert.notNull(tomcatProperties, "");
		Assert.notNull(tomcatConfiguration, "");
		Assert.notNull(tomcatServletWebServerFactory, "");

		boolean hasAjpConnector = false;
		for (final Connector connector : tomcatServletWebServerFactory.getAdditionalTomcatConnectors()) {
			if (TomcatProperties.AJP.PROTOCOL.equalsIgnoreCase(connector.getProtocol())) {
				hasAjpConnector = connector.getPort() == tomcatProperties.getAjp().getPort();
			}
		}
		Assert.isTrue(hasAjpConnector, "");
	}
}
