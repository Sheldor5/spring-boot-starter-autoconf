package at.palata.spring.boot.starter.autoconf.tomcat;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Map;

@Configuration
@ConditionalOnProperty(name = Constants.MODULE_ENABLED_PROPERTY_NAME, havingValue = Constants.MODULE_ENABLED_PROPERTY_VALUE)
@EnableConfigurationProperties(TomcatProperties.class)
@Slf4j
public class TomcatConfiguration {

	private final TomcatProperties properties;

	@Autowired
	public TomcatConfiguration(final TomcatProperties properties) {
		this.properties = properties;
	}

	@PostConstruct
	public void initialize() {
		log.debug("Activating {} ...", Constants.MODULE_NAME);
	}

	@Bean
	public TomcatServletWebServerFactory tomcatServletWebServerFactory() {
		final TomcatServletWebServerFactory tomcatServletWebServerFactory = new TomcatServletWebServerFactory();
		final TomcatProperties.AJP ajp = properties.getAjp();

		if (ajp.isEnabled()) {
			log.debug("Activating AJP Connector: {}", ajp);
			final Connector ajpConnector = new Connector(ajp.getProtocol());
			ajpConnector.setPort(ajp.getPort());

			ajpConnector.setAllowTrace(ajp.isAllowTrace());
			ajpConnector.setAsyncTimeout(ajp.getAsyncTimeout());
			ajpConnector.setEnableLookups(ajp.isEnableLookups());
			ajpConnector.setXpoweredBy(ajp.isXpoweredBy());
			ajpConnector.setProxyName(ajp.getProxyName());
			ajpConnector.setProxyPort(ajp.getProxyPort());
			ajpConnector.setRedirectPort(ajp.getRedirectPort());
			ajpConnector.setScheme(ajp.getScheme());
			ajpConnector.setSecure(ajp.isSecure());
			ajpConnector.setMaxCookieCount(ajp.getMaxCookieCount());
			ajpConnector.setMaxParameterCount(ajp.getMaxParameterCount());
			ajpConnector.setMaxPostSize(ajp.getMaxPostSize());
			ajpConnector.setMaxSavePostSize(ajp.getMaxSavePostSize());
			ajpConnector.setParseBodyMethods(ajp.getParseBodyMethods());
			ajpConnector.setUseIPVHosts(ajp.isUseIPVHosts());
			ajpConnector.setURIEncoding(ajp.getUriEncoding());
			ajpConnector.setUseBodyEncodingForURI(ajp.isUseBodyEncodingForURI());

			ajpConnector.setAttribute("secretRequired", ajp.isSecretRequired());
			if (ajp.getSecret() != null) {
				ajpConnector.setAttribute("secret", ajp.getSecret());
			}

			for (final Map.Entry<String, Object> attribute : ajp.getAdditionalAttributes().entrySet()) {
				ajpConnector.setAttribute(attribute.getKey(), attribute.getValue());
			}

			tomcatServletWebServerFactory.addAdditionalTomcatConnectors(ajpConnector);
		}

		return tomcatServletWebServerFactory;
	}
}
