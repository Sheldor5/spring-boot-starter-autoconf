package at.palata.spring.boot.starter.autoconf.tomcat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@ConfigurationProperties(prefix = Constants.PROPERTY_PREFIX)
public class TomcatProperties {

	private AJP ajp = new AJP();

	@Getter
	@Setter
	@ToString
	public static class AJP {

		public static final String PROTOCOL = "AJP/1.3";

		private boolean enabled = false;
		private String protocol = PROTOCOL;
		private int port = 8009;

		private boolean allowTrace = false;
		private long asyncTimeout = 30000L;
		private boolean enableLookups = false;
		private boolean xpoweredBy = false;
		private String proxyName = null;
		private int proxyPort = 0;
		private int redirectPort = 443;
		private String scheme = "http";
		private boolean secure = false;
		private int maxCookieCount = 200;
		private int maxParameterCount = 10000;
		private int maxPostSize = 2097152;
		private int maxSavePostSize = 4096;
		private String parseBodyMethods = "POST";
		private boolean useIPVHosts = false;
		private String uriEncoding = StandardCharsets.UTF_8.name();
		private boolean useBodyEncodingForURI = false;

		private boolean secretRequired = true;
		private String secret = null;

		private Map<String, Object> additionalAttributes = new HashMap<>();
	}
}
