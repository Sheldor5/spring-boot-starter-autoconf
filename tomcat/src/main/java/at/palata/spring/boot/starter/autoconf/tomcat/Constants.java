package at.palata.spring.boot.starter.autoconf.tomcat;

public class Constants {

	public static final String MODULE_NAME = "Spring Boot Starter Autoconf - Tomcat";

	public static final String PROPERTY_PREFIX = "server.tomcat";
	public static final String MODULE_ENABLED_PROPERTY_NAME = PROPERTY_PREFIX + ".ajp.enabled";
	public static final String MODULE_ENABLED_PROPERTY_VALUE = "true";
}
