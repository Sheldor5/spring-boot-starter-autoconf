# Spring Boot Starter Autoconfigure - Tomcat

## AJP Connector

### Enable the AJP Connector

```
server.tomcat.ajp.enabled=true
```

### AJP Connector Default Configuration

```
server.tomcat.ajp.protocol=AJP/1.3
server.tomcat.ajp.port=8009
server.tomcat.ajp.allowTrace=false
server.tomcat.ajp.asyncTimeout=30000L
server.tomcat.ajp.enableLookups=false
server.tomcat.ajp.xpoweredBy=false
server.tomcat.ajp.proxyName=null
server.tomcat.ajp.proxyPort=0
server.tomcat.ajp.redirectPort=443
server.tomcat.ajp.scheme=http
server.tomcat.ajp.secure=false
server.tomcat.ajp.maxCookieCount=200
server.tomcat.ajp.maxParameterCount=10000
server.tomcat.ajp.maxPostSize=2097152
server.tomcat.ajp.maxSavePostSize=4096
server.tomcat.ajp.parseBodyMethods=POST
server.tomcat.ajp.useIPVHosts=false
server.tomcat.ajp.uriEncoding=UTF-8
server.tomcat.ajp.useBodyEncodingForURI=false
server.tomcat.ajp.secretRequired=true
server.tomcat.ajp.secret=
server.tomcat.ajp.additionalAttributes=
```

### Tomcat Ghostcat Vulnerability

The property `server.tomcat.ajp.secretRequired` is set to `true` by default
to combat the _Tomcat Ghostcat Vulnerability_ ([CVE-2020-1938](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1938))
which requires the property `server.tomcat.ajp.secret` to be set to a non-null, non-empty value.
But because the property `server.tomcat.ajp.secret` is `null` by default, the AJP Connector will fail.

To successfully enable the AJP Connector there are 2 options:

**1. Secure Configuration**

The secure approach is to set a `server.tomcat.ajp.secret`, for example:

```
server.tomcat.ajp.secret=mySecret
```

The value of `server.tomcat.ajp.secret` must also be configured at the proxy which connects to this AJP Connector,
otherwise the AJP Connector will simply drop the connection with HTTP Status Code 403 (only visible in Tomcat's Access Log).

**2. Insecure Configuration**

```
server.tomcat.ajp.secretRequired=false
```

### Security Configuration (REMOTE_USER, etc...)

By default, the AJP Connector only allows connections with whitelisted attributes,
see the `allowedRequestAttributesPattern` attribute at the [Tomcat 9](https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html)
or [Tomcat 8](https://tomcat.apache.org/tomcat-8.5-doc/config/ajp.html) documentation.

A common attribute is the `REMOTE_USER` attribute (a _CGI Environment Variable_ from the proxy, aka `request.getRemoteUser()`)
if the proxy provides an authenticated user to the AJP Connector. By default, the `REMOTE_USER` attribute is not whitelisted!

The simplest, but most insecure approach is to allow any attributes by setting the `allowedRequestAttributesPattern` attribute
as an additional AJP Connector Attribute:

```
server.tomcat.ajp.additionalAttributes.allowedRequestAttributesPattern=*
```

If you don't whitelist the attributes the proxy provides in the request,
the AJP Connector will simply drop the connection with HTTP Status Code 403 (only visible in Tomcat's Access Log).

### Additional AJP Connector Attributes

Additional AJP Connector Attributes can be set by the key-value property `server.tomcat.ajp.additionalAttributes`:

```
server.tomcat.ajp.additionalAttributes.key=value
server.tomcat.ajp.additionalAttributes.foo=bar
server.tomcat.ajp.additionalAttributes.alice=bob
```
