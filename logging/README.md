# Spring Boot Starter Autoconfigure - Logging

## Mapped Diagnostic Context ([MDC](http://logback.qos.ch/manual/mdc.html))

Log HTTP Request data in each log line for easier debugging/auditing.

### Enable the MdcLogEnhancerFilter for your Spring Boot Application

```
logging.mdc.enabled=true
```

### Configuration

#### General

If some data is missing in the HTTP Request, log the configured `nullValue`.

Will simply skip the missing data by default.

Configuring `null` as `nullValue` will print the literal string `null` as value.

```
logging.mdc.nullValue=null
```

#### Headers

Will print `<HEADER_NAME>=<HEADER_VALUE>` in each log line for the configured HTTP Request Headers.

For example:

```
logging.mdc.headers=MY-HEADER, OTHER_HEADER
```

will print `... [MY-HEADER=myValue, OTHER_HEADER=otherValue2] ...` in each log line.

#### Remote User

Will print the `REMOTE_USER` (`request.getRemoteUser()`) from the HTTP Request in each log line as `remoteUser=<REMOTE_USER>`.

```
logging.mdc.remoteUser=true
logging.mdc.remoteUserFieldName=remoteUser
```

#### Spring Security Principal

Will print the `Principal` from Spring Security (`SecurityContextHolder.getContext().getAuthentication().getPrincipal()`) in each log line as `principal=<PRINCIPAL>`.

```
logging.mdc.principal=true
logging.mdc.principalFieldName=principal
```
