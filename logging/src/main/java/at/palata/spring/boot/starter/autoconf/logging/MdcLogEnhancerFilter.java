package at.palata.spring.boot.starter.autoconf.logging;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;

/**
 * @author Michael Palata
 * @date 28.11.2019
 */
@Component
@ConditionalOnProperty(name = Constants.MODULE_ENABLED_PROPERTY_NAME, havingValue = Constants.MODULE_ENABLED_PROPERTY_VALUE)
@EnableConfigurationProperties(MdcLogEnhancerProperties.class)
@Slf4j
public class MdcLogEnhancerFilter implements Filter {

	private final MdcLogEnhancerProperties properties;

	@Autowired
	public MdcLogEnhancerFilter(final MdcLogEnhancerProperties properties) {
		this.properties = properties;
	}

	@PostConstruct
	public void initialize() {
		log.debug("Activating {} ...", Constants.MODULE_NAME);
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		String value;

		// Headers
		for (final String header : properties.getHeaders()) {
			value = request.getHeader(header);
			if (StringUtils.isEmpty(value)) {
				MDC.put(header, properties.getNullValue());
			} else {
				MDC.put(header, value);
			}
			log.trace("Header: {}={}", header, value);
		}

		// Header Mappings
		for (final Map.Entry<String, String> header : properties.getHeaderMappings().entrySet()) {
			value = request.getHeader(header.getKey());
			if (StringUtils.isEmpty(value)) {
				MDC.put(header.getValue(), properties.getNullValue());
			} else {
				MDC.put(header.getValue(), value);
			}
			log.trace("Header Mapping: {}->{}={}", header.getKey(), header.getValue(), value);
		}

		// Remote User
		if (properties.isRemoteUser()) {
			MDC.put(properties.getRemoteUserFieldName(), request.getRemoteUser());
			log.trace("Remote User: {}={}", properties.getRemoteUserFieldName(), request.getRemoteUser());
		}

		// Principal
		if (properties.isPrincipal()) {
			value = null;
			final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			log.trace("Authentication authentication = {}", authentication);
			if (authentication != null) {
				final Object object = authentication.getPrincipal();
				log.trace("Object principal = {}", object);
				if (object != null) {
					log.trace("{} principal = {}", object.getClass().getName(), object);
					if (object instanceof UserDetails) {
						final UserDetails userDetails = (UserDetails) object;
						value = userDetails.getUsername();
					} else if (object instanceof Principal) {
						final Principal principal = (Principal) object;
						value = principal.getName();
					} else {
						value = object.toString();
					}
					MDC.put(properties.getPrincipalFieldName(), value);
				}
			}
			log.trace("Principal: {}={}", properties.getPrincipalFieldName(), value);
		}

		try {
			filterChain.doFilter(servletRequest, servletResponse);
		} finally {
			// Cleanup MDC
			for (final String header : properties.getHeaders()) {
				MDC.remove(header);
			}
			for (final Map.Entry<String, String> header : properties.getHeaderMappings().entrySet()) {
				MDC.remove(header.getKey());
			}
			if (properties.isRemoteUser()) {
				MDC.remove(properties.getRemoteUserFieldName());
			}
			if (properties.isPrincipal()) {
				MDC.remove(properties.getPrincipalFieldName());
			}
		}
	}
}
