package at.palata.spring.boot.starter.autoconf.logging;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.*;

/**
 * @author Michael Palata
 * @date 28.11.2019
 */
@Getter
@Setter
@ConfigurationProperties(prefix = Constants.PROPERTY_PREFIX)
public class MdcLogEnhancerProperties {
	private boolean enabled = false;

	private List<String> headers = new LinkedList<>();
	private Map<String, String> headerMappings = new HashMap<>();

	private boolean remoteUser = false;
	private String remoteUserFieldName = "remoteUser";

	private boolean principal = false;
	private String principalFieldName = "principal";

	private String nullValue = null;
}
