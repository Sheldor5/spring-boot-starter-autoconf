package at.palata.spring.boot.starter.autoconf.logging;

public class Constants {

	public static final String MODULE_NAME = "Spring Boot Starter Autoconf - Logging";

	public static final String PROPERTY_PREFIX = "logging.mdc";
	public static final String MODULE_ENABLED_PROPERTY_NAME = PROPERTY_PREFIX + ".enabled";
	public static final String MODULE_ENABLED_PROPERTY_VALUE = "true";
}
