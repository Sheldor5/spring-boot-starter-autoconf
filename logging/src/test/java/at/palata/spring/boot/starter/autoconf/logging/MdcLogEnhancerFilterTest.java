package at.palata.spring.boot.starter.autoconf.logging;

import at.palata.spring.boot.starter.autoconf.test.TestApplication;
import at.palata.spring.boot.starter.autoconf.test.controller.TestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Michael Palata
 * @date 28.11.2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@AutoConfigureMockMvc
public class MdcLogEnhancerFilterTest {

	@Autowired
	private MdcLogEnhancerProperties properties;

	@Autowired
	private TestController testController;

	@Autowired
	private MockMvc mockMvc;

	@Before
	public void setup() {
		Assert.assertNotNull(testController);
	}

	@WithMockUser
	@Test
	public void test() throws Exception {
		final MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(TestController.PATH);
		final String value = UUID.randomUUID().toString();

		for (String header : properties.getHeaders()) {
			requestBuilder.header(header, value);
		}

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final PrintStream logOutput = new PrintStream(baos);
		final PrintStream old = System.out;
		System.out.flush();
		System.setOut(logOutput);

		final ResultActions resultActions = mockMvc.perform(requestBuilder);

		logOutput.flush();
		System.out.flush();
		System.setOut(old);

		resultActions.andExpect(status().isOk());
		resultActions.andExpect(content().string(TestController.RESPONSE_VALUE));

		final String logLine = baos.toString();
		Assert.assertTrue(logLine.contains(value));
		Assert.assertTrue(logLine.contains(properties.getPrincipalFieldName()));
	}
}
