package at.palata.spring.boot.starter.autoconf.hazelcast;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.hazelcast.config.annotation.web.http.EnableHazelcastHttpSession;

import javax.annotation.PostConstruct;

@Configuration
@ConditionalOnProperty(
		name = {
				Constants.MODULE_ENABLED_PROPERTY_NAME,
				Constants.HTTP_SESSION_PROPERTY_NAME
		},
		havingValue = Constants.MODULE_ENABLED_PROPERTY_VALUE)
@EnableHazelcastHttpSession
@Slf4j
public class HazelcastHttpSessionConfiguration {
	@PostConstruct
	public void initialize() {
		log.debug("HazelcastHttpSession enabled");
	}
}
