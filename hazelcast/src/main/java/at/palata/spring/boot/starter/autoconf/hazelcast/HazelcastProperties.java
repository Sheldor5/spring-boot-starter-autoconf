package at.palata.spring.boot.starter.autoconf.hazelcast;

import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MaxSizeConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michael Palata
 * @date 22.11.2019
 */
@Getter
@Setter
@ConfigurationProperties(prefix = Constants.PROPERTY_PREFIX)
class HazelcastProperties {

	private static final HazelcastMap DEFAULT = new HazelcastMap();

	private boolean enabled = false;

	@NotBlank
	@Value("localhost:${server.port}")
	private String instanceName;
	private String clusterName;
	private String[] interfaces = new String[]{"127.0.0.1"};
	@Min(1024)
	@Max(65536)
	private Integer port = 5701;
	private boolean portAutoIncrement = true;
	private String[] clusterMembers = new String[0];
	private boolean httpSessionReplicationEnabled = false;
	private boolean diagnosticsEnabled = false;
	private Map<String, HazelcastMap> map = new HashMap<>();
	private ManagementCenter managementCenter = new ManagementCenter();

	public HazelcastProperties() {
		this.map.put("default", DEFAULT);
	}

	@Getter
	@Setter
	public static class HazelcastMap {
		private int maxSize = 200;
		private String maxSizePolicy = MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE.name();
		private String evictionPolicy = EvictionPolicy.LRU.name();
		private int timeToLiveSeconds = -1;
	}

	@Getter
	@Setter
	public static class ManagementCenter {
		private boolean enabled = false;
		@NotBlank
		private String url = "http://localhost:8080/hazelcast-mancenter";
	}
}
