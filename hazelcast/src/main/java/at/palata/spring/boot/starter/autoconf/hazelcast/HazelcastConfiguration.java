package at.palata.spring.boot.starter.autoconf.hazelcast;

import com.hazelcast.config.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.session.hazelcast.HazelcastIndexedSessionRepository;
import org.springframework.session.hazelcast.PrincipalNameExtractor;
import org.springframework.session.hazelcast.config.annotation.web.http.EnableHazelcastHttpSession;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * @author Michael Palata
 * @date 22.11.2019
 */
@Configuration
@ConditionalOnProperty(name = Constants.MODULE_ENABLED_PROPERTY_NAME, havingValue = Constants.MODULE_ENABLED_PROPERTY_VALUE)
@EnableConfigurationProperties(HazelcastProperties.class)
@EnableHazelcastHttpSession
@Slf4j
public class HazelcastConfiguration {

	private final HazelcastProperties properties;

	@Autowired
	public HazelcastConfiguration(final HazelcastProperties properties) {
		this.properties = properties;
	}

	@PostConstruct
	public void initialize() {
		log.debug("Activating {} ...", Constants.MODULE_NAME);
	}

	@Bean
	@Primary
	public Config hazelCastConfig() {
		final Config config = new Config();
		config.setInstanceName(properties.getInstanceName());

		/* disable all any-bindings */
		config.setProperty("hazelcast.socket.bind.any", "false");
		config.setProperty("hazelcast.socket.server.bind.any", "false");
		config.setProperty("hazelcast.socket.client.bind.any", "false");
		config.setProperty("hazelcast.diagnostics.enabled", "" + properties.isDiagnosticsEnabled());

		/* Network Configuration */
		final NetworkConfig networkConfig = new NetworkConfig();
		networkConfig.setPort(properties.getPort());
		networkConfig.setPortAutoIncrement(properties.isPortAutoIncrement());

		final InterfacesConfig interfacesConfig = new InterfacesConfig();
		for (final String _interface : properties.getInterfaces()) {
			interfacesConfig.addInterface(_interface);
		}
		interfacesConfig.setEnabled(true);
		networkConfig.setInterfaces(interfacesConfig);

		// Cluster Configuration
		final JoinConfig joinConfig = new JoinConfig();
		if (properties.getClusterName() != null) {
			config.getGroupConfig().setName(properties.getClusterName());

			final TcpIpConfig tcpIpConfig = new TcpIpConfig();

			for (final String clusterMember : properties.getClusterMembers()) {
				tcpIpConfig.addMember(clusterMember);
			}

			tcpIpConfig.setEnabled(true);
			joinConfig.setTcpIpConfig(tcpIpConfig);
		}
		joinConfig.getMulticastConfig().setEnabled(false);

		networkConfig.setJoin(joinConfig);
		config.setNetworkConfig(networkConfig);

		/* HTTP Session Replication */
		if (properties.isHttpSessionReplicationEnabled()) {
			final MapAttributeConfig mapAttributeConfig = new MapAttributeConfig(
					HazelcastIndexedSessionRepository.PRINCIPAL_NAME_ATTRIBUTE,
					PrincipalNameExtractor.class.getName()
			);

			final MapIndexConfig mapIndexConfig = new MapIndexConfig(HazelcastIndexedSessionRepository.PRINCIPAL_NAME_ATTRIBUTE, false);

			config.getMapConfig(HazelcastIndexedSessionRepository.DEFAULT_SESSION_MAP_NAME)
					.addMapAttributeConfig(mapAttributeConfig)
					.addMapIndexConfig(mapIndexConfig);
		}

		/* Hazelcast Maps */
		for (final Map.Entry<String, HazelcastProperties.HazelcastMap> map : properties.getMap().entrySet()) {
			config.addMapConfig(
					new MapConfig()
							.setName(map.getKey())
							.setMaxSizeConfig(new MaxSizeConfig(map.getValue().getMaxSize(), MaxSizeConfig.MaxSizePolicy.valueOf(map.getValue().getMaxSizePolicy())))
							.setEvictionPolicy(EvictionPolicy.valueOf(map.getValue().getEvictionPolicy()))
							.setTimeToLiveSeconds(map.getValue().getTimeToLiveSeconds()));
		}

		/* Hazelcast Management Center */
		final HazelcastProperties.ManagementCenter managementCenter = properties.getManagementCenter();

		final ManagementCenterConfig managementCenterConfig = new ManagementCenterConfig();
		managementCenterConfig.setEnabled(managementCenter.isEnabled());
		managementCenterConfig.setUrl(managementCenter.getUrl());
		config.setManagementCenterConfig(managementCenterConfig);

		return config;
	}
}
