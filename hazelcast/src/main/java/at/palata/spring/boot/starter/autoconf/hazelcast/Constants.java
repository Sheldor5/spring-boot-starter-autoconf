package at.palata.spring.boot.starter.autoconf.hazelcast;

public class Constants {

	public static final String MODULE_NAME = "Spring Boot Starter Autoconf - Hazelcast";

	public static final String PROPERTY_PREFIX = "hazelcast";
	public static final String MODULE_ENABLED_PROPERTY_NAME = PROPERTY_PREFIX + ".enabled";
	public static final String MODULE_ENABLED_PROPERTY_VALUE = "true";
	public static final String HTTP_SESSION_PROPERTY_NAME = PROPERTY_PREFIX + ".httpSessionReplicationEnabled";
	public static final String HTTP_SESSION_PROPERTY_VALUE = "true";
}
