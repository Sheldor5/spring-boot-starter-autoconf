package at.palata.spring.boot.starter.autoconf.hazelcast;

import at.palata.spring.boot.starter.autoconf.hazelcast.HazelcastConfiguration;
import at.palata.spring.boot.starter.autoconf.test.TestApplication;
import com.hazelcast.core.HazelcastInstance;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Michael Palata
 * @date 22.11.2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@ActiveProfiles("NO_HAZELCAST")
public class HazelcastNegativeTest {

	@Autowired(required = false)
	private HazelcastConfiguration hazelcastConfiguration;

	@Autowired(required = false)
	private HazelcastInstance hazelcastInstance;

	@Test
	public void testHazelcastIntegration() {
		Assert.assertNull(hazelcastConfiguration);
		Assert.assertNull(hazelcastInstance);
	}
}
